
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.sql.Date;
import java.util.*;


public class DAOJDBCConcesionari extends DAOConcesionari {

    private static Connection myconn;
    private static ConexioJDBC conexioJDBC = new ConexioJDBC();
    private final ArrayList<Concesionari> concesionaris = new ArrayList<>();
    private final ArrayList<Motos> motosList = new ArrayList<>();
    private static final Logger logger = LogManager.getRootLogger();
    private static String url = "jdbc:mysql://localhost:3306/basededatos";
    private static String user = "root";
    private static String pass = "root";

    /**
     * Crea les taules
     * @throws ExceptionConnect
     */
    @Override
    public void crearTaula() throws ExceptionConnect {
        try {
            myconn = conexioJDBC.connec(url, user, pass);
            DatabaseMetaData dbm = myconn.getMetaData();
            ResultSet tables = dbm.getTables(null, null, "concesionari", null);
            Statement statement = myconn.createStatement();

            if (!tables.next()) {
                String query = " create table concesionari (dni_concesionari varchar(9) primary key,nom_concesionari varchar(15),cantmaxmotos int,esFranquicia boolean);";
                statement.execute(query);
                System.out.println("Taula creada");
            }
            tables = dbm.getTables(null, null, "motos", null);
            if (!tables.next()) {
                String query = " create table motos(nom varchar(15),matricula varchar(7) primary key,data_matriculacio date,dniConces varchar(9),foreign key (dniConces) references concesionari(dni_concesionari));";
                statement.execute(query);
                System.out.println("Taula motos creada");
            }


            System.out.println("La taula ja existeix pots comencçar a treballar amb ella");
        } catch (SQLException e) {
            logger.error("No s'ha conectat correctament ", e);
            logger.getLevel();
            throw new ExceptionConnect("error al conectar");
        } finally {
            myconn = conexioJDBC.close();
        }


    }

    /**
     * elimina les taules ja que la taula de concesionari comparteix valors amb les motos
     * @throws ExceptionConnect
     */
    @Override
    public void borrarTaula() throws ExceptionConnect {
        try {
            myconn = conexioJDBC.connec(url, user, pass);

            DatabaseMetaData dbm = myconn.getMetaData();
            ResultSet tables = dbm.getTables(null, null, "concesionari", null);
            Statement statement = myconn.createStatement();

            if (tables.next()) {
                String query = " drop table concesionari";
                String querymotos = " drop table motos";
                statement.execute(querymotos);
                statement.execute(query);
                System.out.println("Taula de motos eliminada a causa de que les foreign keys han sigut eliminades");
                System.out.println("Taula de concesionari eliminada");
            }
            System.out.println("La taula no existeix");
        } catch (SQLException e) {
            logger.error("No s'ha conectat correctament ", e);
            logger.getLevel();
            throw new ExceptionConnect("error al conectar");
        } finally {
            myconn = conexioJDBC.close();
        }

    }

    /**
     * Emmagatzema tota la bbdd en una arraylist que es retorna
     * @return
     * @throws ExceptionConnect
     */
    @Override
    public List<Concesionari> getAll() throws ExceptionConnect {
        concesionaris.clear();
        try {
            myconn = conexioJDBC.connec(url, user, pass);

            Statement mystmn = myconn.createStatement();
            Motos motos = new Motos();
            Concesionari concesionari = new Concesionari();
            String querymoto = "select * from motos";
            ResultSet resultSet1 = mystmn.executeQuery(querymoto);

            while (resultSet1.next()) {
                Motos motos3=new Motos();
                motos3.setMatricula(resultSet1.getString(2));
                motos3.setNom(resultSet1.getString(1));
                motos3.setData_matriculacio(resultSet1.getString(3));
                motos3.setDniConces(resultSet1.getString(4));
                motosList.add(motos3);
            }

            String queryconcesionari = "select * from concesionari";
            ResultSet resultSet = mystmn.executeQuery(queryconcesionari);

            while (resultSet.next()) {
                Concesionari   concesionari3=new Concesionari();
                concesionari3.setNom_concesionari(resultSet.getString(2));
                concesionari3.setDni_concesionari(resultSet.getString(1));
                concesionari3.setCantmaxmotos(resultSet.getInt(3));
                concesionari3.setEsFranquicia(resultSet.getBoolean(4));

                concesionaris.add(concesionari3);
            }
            ArrayList<Motos> motosCOnces = new ArrayList<>();
            for (int i = 0; i < concesionaris.size(); i++) {
                for (int j = 0; j < motosList.size(); j++) {
                    if (concesionaris.get(i).getDni_concesionari().equals(motosList.get(j).getDniConces())) {
                        motos = new Motos(motosList.get(j).getNom(), motosList.get(j).getMatricula(), motosList.get(j).getData_matriculacio(), motosList.get(j).getDniConces());
                        motosCOnces.add(motos);
                    }
                }
                concesionari=new Concesionari();
                concesionari.setDni_concesionari(concesionaris.get(i).getDni_concesionari());
                concesionari.setNom_concesionari(concesionaris.get(i).getNom_concesionari());
                concesionari.setMagatzem_motos(motosCOnces);
                concesionari.setCantmaxmotos(concesionaris.get(i).getCantmaxmotos());
                concesionari.setEsFranquicia(concesionaris.get(i).isEsFranquicia());
                concesionaris.set(i, concesionari);
            }

            mystmn.close();
            return concesionaris;
        } catch (SQLException e) {
            logger.error("No s'ha conectat correctament ", e);
            logger.getLevel();
            throw new ExceptionConnect("error al conectar");
        } finally {
            myconn = conexioJDBC.close();
        }
    }

    /**
     * insereix el objecte que se li pasa comprobant PK
     * @param clase
     * @throws ExceptionConnect
     * @throws ExceptionRepeat
     */
    @Override
    public void inserir(Concesionari clase) throws ExceptionConnect, ExceptionRepeat {
        try {

            if (primaryKey(clase)) {
                myconn = conexioJDBC.connec(url, user, pass);

                String sentenciaSQL = "insert into concesionari (dni_concesionari,nom_concesionari,cantmaxmotos,esFranquicia)  values (?,?,?,?)";
                PreparedStatement sentenciaPreparada = myconn.prepareStatement(sentenciaSQL);

                sentenciaPreparada.setString(1, clase.getDni_concesionari());
                sentenciaPreparada.setString(2, clase.getNom_concesionari());
                sentenciaPreparada.setInt(3, clase.getCantmaxmotos());
                sentenciaPreparada.setBoolean(4, clase.isEsFranquicia());
                System.out.println("Concesionari inserit correctament.");
                sentenciaPreparada.executeUpdate();

                for (int i = 0; i < clase.getMagatzem_motos().size(); i++) {

                    String queryarray = "insert into motos (nom,matricula,data_matriculacio,dniConces)  values (?,?,?,?)";
                    sentenciaPreparada = myconn.prepareStatement(queryarray);
                    sentenciaPreparada.setString(1, clase.getMagatzem_motos().get(i).getNom());
                    sentenciaPreparada.setString(2, clase.getMagatzem_motos().get(i).getMatricula());
                    if (clase.getMagatzem_motos().get(i).getData_matriculacio().isEmpty()){
                        clase.getMagatzem_motos().get(i).setData_matriculacio("2000-01-01");
                        sentenciaPreparada.setDate(3, Date.valueOf(clase.getMagatzem_motos().get(i).getData_matriculacio()));

                    }
                    sentenciaPreparada.setString(4, clase.getDni_concesionari());
                    sentenciaPreparada.executeUpdate();

                }
            } else {
                System.out.println("Primary Key repetida");
            }

        } catch (ExceptionRepeat e) {
            logger.error("error valor repetit ", e);
            logger.getLevel();
            throw new ExceptionRepeat("error valor repetit");

        } catch (SQLException throwables) {

            logger.error("No s'ha conectat correctament ", throwables);
            logger.getLevel();
            throw new ExceptionConnect("error al conectar");

        } finally {
            myconn = conexioJDBC.close();
        }

    }

    /**
     * retorna el concesionari llistat per id=dni_concesionari
     * @param id
     * @return
     * @throws ExceptionConnect
     */
    @Override
    public Concesionari recuperarPerId(String id) throws ExceptionConnect {
        Concesionari concesionari = new Concesionari();
        try {
            myconn = conexioJDBC.connec(url, user, pass);

            String sentenciaSQL = "select * from concesionari where dni_concesionari= " + id;
            String queryarray = "select * from motos where dniConces= " + id;
            Statement mystmn = myconn.createStatement();
            ResultSet rs = mystmn.executeQuery(sentenciaSQL);

            while (rs.next()) {
                concesionari.setDni_concesionari(rs.getString(1));
                concesionari.setNom_concesionari(rs.getString(2));
                concesionari.setEsFranquicia(rs.getBoolean(4));
                concesionari.setCantmaxmotos(rs.getInt(3));

            }

            rs = mystmn.executeQuery(queryarray);


            ArrayList<Motos> motosArrayList = new ArrayList<>();
            while (rs.next()) {
                Motos motos = new Motos(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4));
                motosArrayList.add(motos);
            }
            concesionari.setMagatzem_motos(motosArrayList);
        } catch (SQLException e) {
            logger.error("No s'ha conectat correctament ", e);
            logger.getLevel();
            throw new ExceptionConnect("error al conectar");
        } finally {
            myconn = conexioJDBC.close();
        }
        return concesionari;
    }

    /**
     * et llista tots els concesionaris de la bbdd i totes les motos
     * @throws ExceptionConnect
     */
    @Override
    public void recuperarTots() throws ExceptionConnect {
        String sentenciaSQL = "select * from concesionari";
        String queryarray = "select * from motos";
        Concesionari concesionari = new Concesionari();


        try {
            myconn = conexioJDBC.connec(url, user, pass);

            Statement mystmn = myconn.createStatement();

            ResultSet rs = mystmn.executeQuery(sentenciaSQL);
            System.out.println("Concesionaris");
            while (rs.next()) {
                concesionari.setDni_concesionari(rs.getString(1));
                concesionari.setNom_concesionari(rs.getString(2));
                concesionari.setEsFranquicia(rs.getBoolean(4));
                concesionari.setCantmaxmotos(rs.getInt(3));
                System.out.println(concesionari);
            }

            rs = mystmn.executeQuery(queryarray);
            System.out.println("\nmotos per concesionari");
            while (rs.next()) {
                Motos motos = new Motos();
                motos.setDniConces(rs.getString(4));
                motos.setMatricula(rs.getString(2));
                System.out.println(motos);
            }
        } catch (SQLException e) {
            logger.error("No s'ha conectat correctament ", e);
            logger.getLevel();

            throw new ExceptionConnect("error al conectar");

        } finally {
            myconn = conexioJDBC.close();
        }

    }

    /**
     * Fa un update dels parametres modificables dels concesionaris
     * @param clase
     * @param params
     * @throws ExceptionConnect
     * @throws ExceptionRepeat
     */
    @Override
    public void update(Concesionari clase, String params) throws ExceptionConnect, ExceptionRepeat {
        try {

            if (primaryKey(clase)) {
                myconn = conexioJDBC.connec(url, user, pass);

                PreparedStatement ps = myconn.prepareStatement("update concesionari set nom_concesionari=?,esFranquicia=? where dni_concesionari=?");

                ps.setString(1, clase.getNom_concesionari());
                ps.setBoolean(2, clase.isEsFranquicia());
                ps.setString(3, params);
                ps.executeUpdate();


                System.out.println("update fet");
            } else {
                System.out.println("Primary key existent");
            }

        } catch (ExceptionRepeat e) {
            logger.error("error valor repetit ", e);
            logger.getLevel();
            throw new ExceptionRepeat("error valor repetit");

        } catch (SQLException e) {
            logger.error("No s'ha conectat correctament ", e);
            logger.getLevel();
            throw new ExceptionConnect("error al conectar");
        } finally {
            myconn = conexioJDBC.close();
        }
    }

    /**
     * elimina el concesionari i totes les motos existens al mateix concesionari
     * @param id
     * @throws ExceptionConnect
     */
    @Override
    public void delete(String id) throws ExceptionConnect {

        try {
            myconn = conexioJDBC.connec(url, user, pass);

            PreparedStatement mystmn = myconn.prepareStatement("delete from concesionari where dni_concesionari=?");
            PreparedStatement mystmndeletemotos = myconn.prepareStatement("delete from motos where dniConces=?");

            mystmndeletemotos.setString(1, id);
            mystmndeletemotos.executeUpdate();
            mystmn.setString(1, id);
            mystmn.executeUpdate();


            System.out.println("s'ha eliminat correctament");
        } catch (SQLException e) {
            logger.error("No s'ha conectat correctament ", e);
            logger.getLevel();
            throw new ExceptionConnect("error al conectar");
        } finally {
            myconn = conexioJDBC.close();
        }
    }

    /**
     *
     * fa un update del dni_concesionari de la moto fent referencia a que s'ha mogut de concesionari
     * @param desti
     * @param matricula
     * @throws ExceptionConnect
     */

    @Override
    public void moureMoto(String desti, String matricula) throws ExceptionConnect {
        try {

            myconn = conexioJDBC.connec(url, user, pass);

            PreparedStatement preparedStatement=myconn.prepareStatement("update motos set dniConces=? where matricula=?");
            preparedStatement.setString(1, desti);
            preparedStatement.setString(2, matricula);
            preparedStatement.executeUpdate();

            System.out.println("moto moguda");

        } catch (SQLException e) {
            logger.error("No s'ha conectat correctament ", e);
            logger.getLevel();
            throw new ExceptionConnect("error al conectar");
        } finally {
            myconn = conexioJDBC.close();
        }
    }
/**

    /**
     *  Crida al metode getAll() per emmagatzemar tota la bbdd a una arraylist
     *  y compoba que la PK del Objecte que se li pasa per valor no estigui repetida
     *  return boolean
     * @param concesionari
     * @return boolean
     */
    @Override
    public boolean primaryKey(Concesionari concesionari) {
        boolean ret = true;
        try {
            myconn = conexioJDBC.connec(url, user, pass);

            getAll();
            for (int i = 0; i < concesionaris.size(); i++) {
                if (concesionaris.get(i).getDni_concesionari().equals(concesionari.getDni_concesionari())) {
                    ret = false;
                }
            }
        } catch (ExceptionConnect throwables) {
            throwables.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            myconn = conexioJDBC.close();
        }
        return ret;
    }
}
