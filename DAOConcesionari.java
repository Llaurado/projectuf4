import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public  class DAOConcesionari implements DAO<Concesionari>{


    @Override
    public Optional<Concesionari> get(long id) {
        return Optional.empty();
    }

    @Override
    public List<Concesionari> getAll() throws   ExceptionConnect {


        return null;
    }


    @Override
    public void inserir(Concesionari clase) throws ExceptionConnect, ExceptionRepeat {

    }

    @Override
    public Concesionari recuperarPerId(String id) throws   ExceptionConnect {

        return null;
    }

    @Override
    public void getConnection(String url, String user, String pass) throws ExceptionConnect {

    }

    @Override
    public void closeConnection() throws ExceptionConnect {

    }


    @Override
    public void recuperarTots() throws   ExceptionConnect {

    }

    @Override
    public void update(Concesionari clase, String params) throws ExceptionConnect, ExceptionRepeat {

    }

    @Override
    public void delete(String id) throws   ExceptionConnect {

    }

    @Override
    public void crearTaula() throws SQLException {

    }

    @Override
    public void borrarTaula() throws   ExceptionConnect {

    }

    @Override
    public void moureMoto(String desti, String matricula) throws  ExceptionConnect {

    }

    @Override
    public boolean primaryKey(Concesionari concesionari) {
        return false;
    }


}
