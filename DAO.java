import java.net.ConnectException;
import java.sql.SQLException;
import java.util.*;
import java.util.Optional;

public interface DAO<T> {

    Optional<T> get(long id);

    List<T> getAll() throws ExceptionConnect;


    void inserir(T t) throws SQLException;

    T recuperarPerId(String id) throws   ExceptionConnect;

    void getConnection(String url,String user,String pass) throws ExceptionConnect;
    void closeConnection() throws ExceptionConnect;

    void recuperarTots() throws   ExceptionConnect;

    void update(T t, String params) throws ExceptionConnect, ExceptionRepeat;

    void delete(String id) throws   ExceptionConnect;

    void crearTaula() throws SQLException;

    void borrarTaula() throws   ExceptionConnect;
    void moureMoto(String dni,String matricula) throws  ExceptionConnect;
    boolean primaryKey(T t);
}