import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public class DAOMotos implements  DAO<Motos>{

    @Override
    public Optional<Motos> get(long id) {
        return Optional.empty();
    }

    @Override
    public List<Motos> getAll() throws   ExceptionConnect {
        return null;
    }



    @Override
    public void inserir(Motos estudiants) throws SQLException {

    }

    @Override
    public Motos recuperarPerId(String id) throws   ExceptionConnect {

        return null;
    }

    @Override
    public void getConnection(String url, String user, String pass) throws ExceptionConnect {

    }

    @Override
    public void closeConnection() throws ExceptionConnect {

    }


    @Override
    public void recuperarTots() throws   ExceptionConnect {

    }

    @Override
    public void update(Motos motos, String params) throws   ExceptionConnect {

    }

    @Override
    public void delete(String id) throws   ExceptionConnect {

    }

    @Override
    public void crearTaula() throws   ExceptionConnect {

    }

    @Override
    public void borrarTaula() throws   ExceptionConnect {

    }

    @Override
    public void moureMoto(String desti, String matricula) throws ExceptionConnect {

    }

    @Override
    public boolean primaryKey(Motos motos) {
        return false;
    }


}
