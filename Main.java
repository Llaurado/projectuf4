


import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class Main {

    private static final Logger logger = LogManager.getRootLogger();
    private static final DAOConcesionari daoConcesionari = new DAOJDBCConcesionari();
    private static final DAOMotos daoMotos = new DAOJDBCMotos();
    private static String url = "jdbc:mysql://localhost:3306/basededatos";
    private static String user = "root";
    private static String pass = "root";


    public static int menu1() throws ExceptionConnect {
        Scanner sc = new Scanner(System.in);
        System.out.println("------Menu1-------");
        System.out.println("0.Sortir");
        System.out.println("1.Motos");
        System.out.println("2.Concesionaris");
        int menu = sc.nextInt();
        try {
            if (menu == 1) {
                menu2();
            } else if (menu == 2) {
                menu3();
            } else if (menu == 0) {
                System.out.println("Programa finalitzat");
            }
        } catch (Exception e) {
            logger.error("No s'ha conectat correctament ", e);
            logger.getLevel();
            throw new ExceptionConnect("error al conectar");
        } finally {
            daoMotos.closeConnection();
            daoConcesionari.closeConnection();
        }
        return menu;

    }

    public static void llistardniconces() {
        try {

            daoConcesionari.getAll();
            for (int i = 0; i < daoConcesionari.getAll().size(); i++) {
                System.out.println("Concesionari " + i + " dni " + daoConcesionari.getAll().get(i).getDni_concesionari());
            }
        } catch (ExceptionConnect throwables) {
            throwables.printStackTrace();
        }
    }

    public static void llistarMatr() {
        try {

            ArrayList<Motos> motosArrayList = new ArrayList<>(daoMotos.getAll());

            for (int i = 0; i < daoMotos.getAll().size(); i++) {

                System.out.println("Moto " + i + " matri " + daoMotos.getAll().get(i).getMatricula());
            }
        } catch (ExceptionConnect throwables) {
            throwables.printStackTrace();
        }
    }

    public static void menu2() throws ExceptionConnect {
        System.out.println("------Menu2-------");
        System.out.println("0.Sortir");
        System.out.println("1.Crear la taula moto");
        System.out.println("2.Borrar la taula moto");
        System.out.println("3.Añadir una moto");
        System.out.println("4.Llistar una moto per matricula");
        System.out.println("5.Llistar totes les motos");
        System.out.println("6.Modificar una moto");
        System.out.println("7.Borrar una moto");
        System.out.println("8.Pasar totes les motos a una llista");


        Scanner sc = new Scanner(System.in);

        int a = 1;
        try {

            while (a != 0) {
                int opcio = sc.nextInt();
                Scanner scmotos = new Scanner(System.in);

                switch (opcio) {

                    case 0:
                        a = 0;
                        break;
                    case 1:
                        daoMotos.crearTaula();
                        Thread.sleep(5000);

                        break;
                    case 2:
                        daoMotos.borrarTaula();
                        Thread.sleep(5000);

                        break;
                    case 3:
                        System.out.println("Dades de la moto a inserir:");
                        llistardniconces();
                        System.out.println("A quin concesionari pertany (dni conces)");
                        sc.nextLine();
                        String dni_conces = sc.nextLine();
                        System.out.println("Model de la moto");
                        String nom = scmotos.nextLine();
                        System.out.println("Data de matriculacio format(YYYY-MM-DD)");
                        String data_matri = scmotos.nextLine();
                        System.out.println("Matricula de la moto");
                        String matricula = scmotos.next();
                        System.out.println(dni_conces);
                        Motos motos = new Motos(nom, matricula, data_matri, dni_conces);
                        daoMotos.inserir(motos);
                        Thread.sleep(5000);

                        break;
                    case 4:
                        System.out.println("Matricula de la moto");
                        matricula = scmotos.next();
                        System.out.println(daoMotos.recuperarPerId(matricula));
                        Thread.sleep(5000);

                        break;
                    case 5:
                        daoMotos.recuperarTots();
                        Thread.sleep(5000);

                        break;
                    case 6:
                        System.out.println("Dades de la moto a modificar:");

                        System.out.println("Nou model de la moto");
                        nom = scmotos.nextLine();
                        System.out.println("Nova matricula de la moto");
                        matricula = scmotos.next();


                        motos = new Motos();
                        motos.setNom(nom);
                        motos.setMatricula(matricula);
                        llistarMatr();
                        System.out.println("Matricula antiga");
                        String matr = scmotos.next();

                        daoMotos.update(motos, matr);
                        Thread.sleep(5000);

                        break;
                    case 7:
                        System.out.println("Matricula de la moto a borrar");
                        llistarMatr();
                        matricula = scmotos.next();
                        daoMotos.delete(matricula);
                        Thread.sleep(5000);

                        break;
                    case 8:
                        daoMotos.getAll();
                        Thread.sleep(5000);

                        break;
                    default:
                        System.out.println("opcio no valida tornara al menu1");
                        a = 0;

                }
                if (a == 0) {
                    menu1();
                }
                {
                    menu2();
                }

                daoMotos.closeConnection();
            }
        } catch (Exception e) {
            a = 0;
            logger.error("No s'ha conectat correctament ", e);
            logger.getLevel();
            throw new ExceptionConnect("error al conectar");
        }
    }


    public static void menu3() throws ExceptionConnect {
        System.out.println("------Menu3-------");
        System.out.println("0.Sortir");
        System.out.println("1.Crear la taula concesionari");
        System.out.println("2.Borrar la taula concesionari");
        System.out.println("3.Añadir un concesionari");
        System.out.println("4.Llistar un concesionari per dni");
        System.out.println("5.Llistar tots els concesionari");
        System.out.println("6.Modificar un concesionari");
        System.out.println("7.Borrar una concesionari");
        System.out.println("8.Pasar tots els concesionari a una llista");
        System.out.println("9.Moure moto de concesionari");

        Scanner sc = new Scanner(System.in);
        int a = 1;


        try {
            while (a != 0) {
                int opcio = sc.nextInt();
                switch (opcio) {
                    case 0:
                        a = 0;
                        break;
                    case 1:
                        daoConcesionari.crearTaula();
                        Thread.sleep(5000);

                        break;
                    case 2:
                        daoConcesionari.borrarTaula();
                        Thread.sleep(5000);

                        break;
                    case 3:
                        System.out.println("Dades del concesionari a inserir:");
                        System.out.println("Nom del concesionari");
                        sc.nextLine();
                        String nom = sc.nextLine();

                        System.out.println("Es franquicia (true or false)");
                        boolean franquicia = Boolean.parseBoolean(sc.nextLine());
                        System.out.println("dni del concesionari");
                        String dni_conces = sc.next();
                        System.out.println("Llistat de motos a inserir, escogeix cuantes motos desitges inserir max 10");
                        int max = sc.nextInt();


                        List<Motos> motos = new ArrayList<>();
                        for (int i = 0; i < max; i++) {

                            System.out.println("Model moto");
                            sc.nextLine();
                            String nom_moto = sc.nextLine();
                            System.out.println("Data de matriculacio(YYYY-MM-DD)");
                            String data_matri = sc.nextLine();
                            System.out.println("Matricula de la moto");
                            String matricula = sc.next();
                            Motos motos1 = new Motos(nom_moto, matricula, data_matri, dni_conces);

                            motos.add(motos1);
                            System.out.println("Moto num :" + i + 1);
                        }

                        Concesionari concesionari = new Concesionari(dni_conces, nom, motos, franquicia, 10);
                        daoConcesionari.inserir(concesionari);
                        Thread.sleep(5000);

                        break;
                    case 4:
                        llistardniconces();
                        System.out.println("Dni del concesionari a cercar");
                        dni_conces = sc.next();
                        System.out.println(daoConcesionari.recuperarPerId(dni_conces));
                        Thread.sleep(5000);

                        break;
                    case 5:
                        daoConcesionari.recuperarTots();
                        Thread.sleep(5000);

                        break;
                    case 6:
                        System.out.println("Dades del nou  concesionari ");
                        System.out.println("Es Franquicia (true or false)");
                        dni_conces = sc.next();
                        System.out.println("Nom del concesionari");
                        nom = sc.next();

                        Concesionari concesionari1 = new Concesionari();
                        concesionari1.setDni_concesionari(dni_conces);
                        concesionari1.setNom_concesionari(nom);
                        llistardniconces();
                        System.out.println("Dni Concesionari a modificar");
                        String dni = sc.next();

                        daoConcesionari.update(concesionari1, dni);
                        Thread.sleep(5000);

                        break;
                    case 7:
                        llistardniconces();
                        System.out.println("Dni del concesionari a borrar");
                        dni = sc.next();
                        daoConcesionari.delete(dni);
                        Thread.sleep(5000);

                        break;
                    case 8:
                        daoConcesionari.getAll();
                        Thread.sleep(5000);

                        break;
                    case 9:
                        llistarMatr();
                        System.out.println("Quina moto desitja moure de concesionari (matr)");
                        String matricula = sc.next();
                        llistardniconces();
                        System.out.println("Dni concesionari desti");
                        dni_conces = sc.next();
                        daoConcesionari.moureMoto(dni_conces, matricula);
                        break;
                    default:
                        System.out.println("opcio no valida tornara al menu1");
                        a = 0;

                }
                if (a == 0) {
                    menu1();
                }
                {
                    menu3();
                }
                daoConcesionari.closeConnection();
            }
        } catch (Exception e) {
            a = 0;
            logger.error("No s'ha conectat correctament ", e);
            logger.getLevel();
            throw new ExceptionConnect("error al conectar");
        }

    }


    public static void main(String[] args) throws ExceptionConnect {
        try {
//            llistarMatr();
//            llistardniconces();

            menu1();
        } catch (Exception e) {
            logger.error("No s'ha conectat correctament ", e);
            logger.getLevel();
            throw new ExceptionConnect("error al conectar");
        }


    }
}