
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class DAOJDBCMotos extends DAOMotos {
    private List<Motos> motos = new ArrayList<>();
    private static final Logger logger = LogManager.getRootLogger();
    private static Connection myconn;
    private static String url="jdbc:mysql://localhost:3306/basededatos";
    private static String user="root";
    private static String pass="root";
    private static ConexioJDBC conexioJDBC=new ConexioJDBC();


    /**
     *
     * @throws ExceptionConnect
     */
    @Override
    public void crearTaula() throws ExceptionConnect {
        try {
            myconn=conexioJDBC.connec(url,user,pass);

            if (!myconn.getAutoCommit()) {
                throw new SQLException("error al conectar");
            }

            DatabaseMetaData dbm = myconn.getMetaData();
            ResultSet tables = dbm.getTables(null, null, "motos", null);
            Statement statement = myconn.createStatement();


            if (!tables.next()) {
                String query = " create table motos(nom varchar(15),matricula varchar(7) primary key,data_matriculacio date,dniConces varchar(9),foreign key (dniConces) references concesionari(dni_concesionari));";
                statement.execute(query);
                System.out.println("Taula creada");
            }
            System.out.println("La taula ja existeix pots comencçar a treballar amb ella");
        } catch (Exception e) {
            logger.error("No s'ha conectat correctament ", e);
            logger.getLevel();
            throw new ExceptionConnect("error al conectar");
        }finally {
            myconn = conexioJDBC.close();
        }


    }

    /**
     *
     * @throws ExceptionConnect
     */
    @Override
    public void borrarTaula() throws ExceptionConnect {
        try {
            myconn=conexioJDBC.connec(url,user,pass);

            if (!myconn.getAutoCommit()) {
                throw new SQLException("error al conectar");
            }

            DatabaseMetaData dbm = myconn.getMetaData();
            ResultSet tables = dbm.getTables(null, null, "motos", null);
            Statement statement = myconn.createStatement();

            if (tables.next()) {
                String query = " drop table motos";
                statement.execute(query);
                System.out.println("Taula eliminada");
            }
            System.out.println("La taula no existeix");
        } catch (Exception e) {
            logger.error("No s'ha conectat correctament ", e);
            logger.getLevel();
            throw new ExceptionConnect("error al conectar");
        }finally {
            myconn = conexioJDBC.close();
        }

    }

    /**
     *
     * @return
     * @throws ExceptionConnect
     */
    @Override
    public List<Motos> getAll() throws ExceptionConnect {
        try {
            myconn=conexioJDBC.connec(url,user,pass);

            if (!myconn.getAutoCommit()) {
                throw new SQLException("error al conectar");
            }
            motos.clear();
            Statement mystmn = myconn.createStatement();
            String query = "select * from motos";
            ResultSet rs = mystmn.executeQuery(query);
            while (rs.next()) {
                Motos motos1 = new Motos();
                motos1.setMatricula(rs.getString(2));
                motos1.setNom(rs.getString(1));
                motos1.setData_matriculacio(rs.getString(3));
                motos1.setDniConces(rs.getString(4));
                motos.add(motos1);
            }
        } catch (Exception e) {
            logger.error("No s'ha conectat correctament ", e);
            logger.getLevel();
            throw new ExceptionConnect("error al conectar");
        }
        finally {
            myconn = conexioJDBC.close();

        }

        return motos;

    }

    /**
     *
     * @param motos
     * @throws SQLException
     */
    @Override
    public void inserir(Motos motos) throws SQLException {
        try {

            if (primaryKey(motos)) {
                String sentenciaSQL = "insert into motos (nom,matricula,data_matriculacio,dniConces)  values (?,?,?,?)";
                myconn=conexioJDBC.connec(url,user,pass);

                PreparedStatement sentenciaPreparada = myconn.prepareStatement(sentenciaSQL);
                sentenciaPreparada.setString(1, motos.getNom());
                sentenciaPreparada.setString(2, motos.getMatricula());
                if (motos.getData_matriculacio().isEmpty()){
                    motos.setData_matriculacio("2000-01-01");
                    sentenciaPreparada.setDate(3, Date.valueOf(motos.getData_matriculacio()));
                }
                sentenciaPreparada.setDate(3, Date.valueOf(motos.getData_matriculacio()));

                if (!(motos.getDniConces().isEmpty())) {
                    sentenciaPreparada.setString(4, motos.getDniConces());
                } else {
                    System.out.println("No existeix cap concesionari o no s'ha introduit dni del concesionari");
                 return;
                }
                sentenciaPreparada.executeUpdate();

                System.out.println("Client inserit correctament.");

            } else {
                System.out.println("primary key repetida");
            }
        } catch (SQLException e) {
            logger.error("No s'ha conectat correctament ", e);
            logger.getLevel();
            throw new ExceptionConnect("error al conectar");
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }finally {
            myconn = conexioJDBC.close();
        }
    }

    /**
     *
     * @param id
     * @return
     * @throws ExceptionConnect
     */
    @Override
    public Motos recuperarPerId(String id) throws ExceptionConnect {
        Motos motos = null;
        try {
            myconn=conexioJDBC.connec(url,user,pass);

            String sentenciaSQL = "select * from motos where matricula=" + id;

            Statement mystmn = myconn.createStatement();

            ResultSet rs = mystmn.executeQuery(sentenciaSQL);
            while (rs.next()) {
                motos = new Motos(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4));
            }
        } catch (Exception e) {
            logger.error("No s'ha conectat correctament ", e);
            logger.getLevel();
            throw new ExceptionConnect("error al conectar");
        }finally {
            myconn = conexioJDBC.close();
        }
        return motos;
    }

    /**
     *
     * @throws ExceptionConnect
     */
    @Override
    public void recuperarTots() throws ExceptionConnect {
        Motos motos = null;

        try {
            myconn=conexioJDBC.connec(url,user,pass);

            if (!myconn.getAutoCommit()) {
                throw new SQLException("error al conectar");
            }

            String sentenciaSQL = "select * from motos";

            Statement mystmn = myconn.createStatement();

            ResultSet rs = mystmn.executeQuery(sentenciaSQL);

            while (rs.next()) {
                motos = new Motos(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4));

                System.out.println(motos);
            }
        } catch (Exception e) {
            logger.error("No s'ha conectat correctament ", e);
            logger.getLevel();
            throw new ExceptionConnect("error al conectar");
        }finally {
            myconn = conexioJDBC.close();
        }
    }

    /**
     *
     * @param motos
     * @param params
     * @throws ExceptionConnect
     */
    @Override
    public void update(Motos motos, String params) throws ExceptionConnect {
        try {
            if (primaryKey(motos)) {
                if (!myconn.getAutoCommit()) {
                    throw new SQLException("error al conectar");
                }
                myconn=conexioJDBC.connec(url,user,pass);

                PreparedStatement preparedStatement = myconn.prepareStatement("update motos set nom=?,matricula=?where matricula=?");
                preparedStatement.setString(1, motos.getNom());
                preparedStatement.setString(2, motos.getMatricula());
                preparedStatement.setString(3, params);
                preparedStatement.executeUpdate();
                System.out.println("update fet");
            } else {
                System.out.println("primary key existent");
            }
        } catch (Exception e) {
            logger.error("No s'ha conectat correctament ", e);
            logger.getLevel();
            throw new ExceptionConnect("error al conectar");
        }finally {
            myconn = conexioJDBC.close();
        }
    }

    /**
     *
     * @param id
     * @throws ExceptionConnect
     */
    @Override
    public void delete(String id) throws ExceptionConnect {
        try {
            myconn=conexioJDBC.connec(url,user,pass);

            if (!myconn.getAutoCommit()) {
                throw new SQLException("error al conectar");
            }


            PreparedStatement mystmn = myconn.prepareStatement("delete from motos where matricula=?");
            mystmn.setString(1, id);
            mystmn.executeUpdate();
            System.out.println("s'ha eliminat la moto");
        } catch (Exception e) {
            logger.error("No s'ha conectat correctament ", e);
            logger.getLevel();
            throw new ExceptionConnect("error al conectar");
        }finally {
            myconn = conexioJDBC.close();
        }
    }


    /**
     *
     * @param moto
     * @return
     */
    @Override
    public boolean primaryKey(Motos moto) {
        boolean ret = true;
        try {
            getAll();
            for (int i = 0; i < motos.size(); i++) {
                if (motos.get(i).getMatricula().equals(moto.getMatricula())) {
                    ret = false;
                }
            }
        } catch (ExceptionConnect throwables) {
            throwables.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return ret;
    }
}
