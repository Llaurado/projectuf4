import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConexioJDBC implements Conexio {
    Connection myconn;

    /**
     * connects to JDBC
     * @param url
     * @param user
     * @param pass
     * @return
     */
    @Override
    public Connection connec(String url,String user,String pass)  {
        try {

            myconn = DriverManager.getConnection(url, user, pass);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return myconn;
    }

    /**
     * Close connection
     * @return
     */
    @Override
    public Connection close() {
        try {
            myconn.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return myconn;
    }
}
